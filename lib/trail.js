/*
An object that keeps track of a set of lines and connects them together.
*/

class Trail {
  constructor(x1, y1, x2, y2, max, col1=0, col2=0) {
    this.points = [[x1, y1, col1], [x2, y2, col2]]
    this.maxlen = max
  }

  addPoint(x, y, col=255) {
    this.points.push([x, y, col])
    if (this.points.length > this.maxlen) {
      this.points = this.points.slice(1)
    }
  }

  draw() {
    let prevPoint = this.points[0]
    for (let i = 1; i < this.points.length; i++) {
      stroke(prevPoint[2])
      strokeWeight(2)
      line(prevPoint[0], prevPoint[1], this.points[i][0], this.points[i][1])
      prevPoint = this.points[i]
    }
  }

  drawBezier() {
    beginShape()

    for (let i = 1; i < this.points.length; i++) {
      curveVertex(this.points[i][0], this.points[i][1])
    }

    endShape()
  }
}

/*
A better object that keeps track of a set of lines and connects them together.
*/

class Trail2 {
  constructor(points=[], max=9999999) {
    this.points = points
    this.maxlen = max
  }

  addPoint(x, y, col=255) {
    this.points.push([x, y, col])
    if (this.points.length > this.maxlen) {
      this.points = this.points.slice(1)
    }
  }

  draw() {
    let prevPoint = this.points[0]
    for (let i = 1; i < this.points.length; i++) {
      stroke(prevPoint[2])
      line(prevPoint[0], prevPoint[1], this.points[i][0], this.points[i][1])
      prevPoint = this.points[i]
    }
  }

  drawBezier() {
    beginShape()

    for (let i = 1; i < this.points.length; i++) {
      curveVertex(this.points[i][0], this.points[i][1])
    }

    endShape()
  }
}
