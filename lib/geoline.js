/*
An object that represents a line according to y = mx + b and can be
moved with a velocity.
*/

class Line {
  constructor(startx, starty, grad, col, decay) {
    this.x1 = startx
    this.y1 = starty
    this.m = grad
    this.c = col
    this.vx = random(-2, 2)
    this.vy = random(-2, 2)
    this.decay_sec = decay
    this.ticks = 0
  }

  f(x) {
    return this.m * (x - this.x1) + this.y1
  }

  old() {
    return (this.ticks/60) > this.decay_sec
  }

  update() {
    this.x1 += this.vx
    this.y1 += this.vy
  }

  draw() {
    strokeWeight(3)
    stroke(this.c)
    line(0, this.f(0), width, this.f(width))
    this.ticks++
  }
}
