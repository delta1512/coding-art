/* 
Tiles object
---
Makes the process of tiling the canvas super easy.

Splits the canvas into n by m tiles and fills it completely.
Provides helpful interfaces for performing tiled-algorithms like recursive backtracking.
*/

class Tile {
    constructor(n_tiles_width, n_tiles_height, idx_x, idx_y) {
        this.tiles_w = n_tiles_width
        this.tiles_h = n_tiles_height
        this.idx_x = idx_x
        this.idx_y = idx_y
        this.x = (width / this.tiles_w) * idx_x
        this.y = (height / this.tiles_h) * idx_y
        this.w = (width / this.tiles_w)
        this.h = (height / this.tiles_h)
        this.marker = false
        this.pre_draw = function() {}
        this.post_draw = function() {}
    }

    getCenter() {
        return [this.x + this.w / 2, this.y + this.h / 2]
    }

    idxDist(otherTile) {
        return dist(this.idx_x, this.idx_y, otherTile.idx_x, otherTile.idx_y)
    }

    centerDist(otherTile) {
        let centerA = this.getCenter()
        let centerB = otherTile.getCenter()
        return dist(centerA[0], centerA[1], centerB[0], centerB[1])
    }

    draw() {
        this.pre_draw()
        rect(this.x, this.y, this.w, this.h)
        this.post_draw()
    }
}

class Tiles {
    constructor(n_tiles_width = 1, n_tiles_height = 1) {
        this.tiles_w = n_tiles_width
        this.tiles_h = n_tiles_height
        this.tiles = new Array()

        for (let i = 0; i < this.tiles_h; i++) {
            this.tiles.push(new Array())
            for (let j = 0; j < this.tiles_w; j++) {
                this.tiles[i].push(new Tile(this.tiles_w, this.tiles_h, j, i))
            }
        }
    }

    getTile(x, y) {
        return this.tiles[y][x]
    }

    getNeighboursByCoord(x, y) {
        /* 
            Returns an array of 4 Tile objects or null if an edge is hit using indecies as ID.
            The order of the array is respective to the direction of the neighbour:
            0 - top
            1 - right
            2 - bottom
            3 - left
        */
        return [
            [0, -1],
            [1, 0],
            [0, 1],
            [-1, 0]
        ].map(offs => {
            let xoff = offs[0]
            let yoff = offs[1]

            if (x + xoff >= this.tiles_w || x + xoff < 0) {
                return null
            } else if (y + yoff >= this.tiles_h || y + yoff < 0) {
                return null
            } else {
                return this.getTile(x + xoff, y + yoff)
            }
        })
    }

    getNeighboursByTile(tile) {
        return this.getNeighboursByCoord(tile.idx_x, tile.idx_y)
    }

    get8NeighboursByCoord(x, y) {
        /* Same as getNeighboursByCoord but for the entire 8 tiles surrounding x,y starting from the top */
        return [
            [0, -1],
            [1, -1],
            [1, 0],
            [1, 1],
            [0, 1],
            [-1, 1],
            [-1, 0],
            [-1, -1],
        ].map(offs => {
            let xoff = offs[0]
            let yoff = offs[1]

            if (x + xoff >= this.tiles_w || x + xoff < 0) {
                return null
            } else if (y + yoff >= this.tiles_h || y + yoff < 0) {
                return null
            } else {
                return this.getTile(x + xoff, y + yoff)
            }
        })
    }

    get8NeighboursByTile(tile) {
        return this.get8NeighboursByCoord(tile.idx_x, tile.idx_y)
    }

    draw() {
        this.tiles.forEach(tile_row => {
            tile_row.forEach(tile => {
                tile.draw()
            })
        })
    }
}