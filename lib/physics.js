/*
An object that represents a 2D object with physical properties of motion.
*/

const G = 6.67408e-11
const TOP = 0
const RIGHT = 1
const BOTTOM = 2
const LEFT = 3


class PhysObj {
  constructor(startx=0, starty=0, mass=1, col=color(255), froz=false, startvx=random(-2, 2), startvy=random(-2, 2)) {
    this.pos = createVector(startx, starty)
    this.v = createVector(startvx, startvy)
    //this.v = p5.Vector.fromAngle(toRadians(45), 0)
    this.a = createVector(0, 0)
    this.f = createVector(0, 0)
    this.mass = mass
    this.col = col
    this.frozen = froz
    this.constrain = true
    this.terminalV = Infinity
    this.creationTime = new Date
    this.creationTime = this.creationTime.getTime()
    this.ttl = Infinity // In milliseconds
    //TODO: Constrain should pass the direction in which the object was constrained
    this.onConstrain = function defaultConstrain(dir) {}
  }

  freezeOnConstrain(dir) {
    if (dir == BOTTOM) {
      this.frozen = true
    }
  }

  applyField(field) {
    this.a.add(field.a)
    this.f.add(field.f)
  }

  //applies the velocity and finds the new position of the particle
  calcPos() {
    this.pos.add(this.v)
  }

  //applies acceleration to determine the new velocity
  calcVelocity() {
    this.v.add(this.a)
    this.v.limit(this.terminalV)
  }

  //applies force to determine the new acceleration
  calcAcceleration() {
    this.a.add(createVector(this.f.x / this.mass, this.f.y / this.mass))
  }

  update() {
    //the particle will not move if it is frozen
    //this means no calculations occur
    if (!this.frozen) {
        this.calcAcceleration()
        this.calcVelocity()
        this.calcPos()
    }

    if (this.constrain) {
      //Check whether the particle hits the left or right side of the viewport.
      if (this.pos.x < 0) {
        this.v.x *= -1
        this.pos.x = 0
        this.onConstrain(LEFT)
      } else if (this.pos.x > width) {
        this.v.x *= -1
        this.pos.x = width
        this.onConstrain(RIGHT)
      }

      //Check whether the particle hits the top or bottom of the viewport
      if (this.pos.y < 0) {
        this.v.y *= -1
        this.pos.y = 0
        this.onConstrain(TOP)
      } else if (this.pos.y > height) {
        this.v.y *= -1
        this.pos.y = height
        this.onConstrain(BOTTOM)
      }
    }

    this.a = createVector(0, 0)
    this.f = createVector(0, 0)
  }

  isAlive() {
    let currentTime = new Date
    return this.creationTime + this.ttl > currentTime.getTime()
  }

  show() {
    //draw the particle as a circle
    fill(this.col)
    noStroke()
    ellipse(this.pos.x, this.pos.y, 20)
  }
}


//Force field object
class Field {
  constructor(aMag=0, fMag=0, angle=0) {
    this.a = p5.Vector.fromAngle(toRadians(angle), aMag)    //acceleration
    this.f = p5.Vector.fromAngle(toRadians(angle), fMag)    //force
    this.theta = angle
  }
}


//Calculates 2D gravitational attraction between two objects and returns a field
//representing the attraction
function gravity(cobj, robj) {
  rtheta = 0 //0
  if (robj.pos.x > cobj.pos.x) {
    if (robj.pos.y > cobj.pos.y) {
      //console.log("Q1")
      rtheta = 180
    } else {
      //console.log("Q4")
      rtheta = 180
    }
  } else if (robj.pos.y > cobj.pos.y) {
    //console.log("Q2")
    rtheta = 0
  }

  d = dist(cobj.pos.x, robj.pos.x, cobj.pos.y, robj.pos.y)
  theta = atan((cobj.pos.y - robj.pos.y)/(cobj.pos.x - robj.pos.x))
  f = (G * cobj.mass * robj.mass) / pow(d, 2)

  return new Field(0, f, toDegrees(theta)+rtheta)
}
