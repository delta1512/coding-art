/*
A set of higher-functionality math objects and operations.
*/
var _getNoise_pos = 1


//Implementation of an arithmetic sequence in the form Tn = a + d(n-1)
class arithSeq {
  constructor(start, diff, n=0) {
    this.a = start
    this.d = diff
    this.n = n
  }

  //gets the nth term (intended to be called by the object itself)
  tn(n=this.n) {
    return this.a + this.d * (n - 1)
  }

  //gets the sum to and including the nth term
  sn(n=this.n) {
    return (n/2) * (this.a + this.tn(n))
  }

  //gets the next term in the sequence and automatically increments itself
  next() {
    let val = this.tn()
    this.n++
    return val
  }
}



//Implementation of a geometric sequence in the form of Tn = a(r)^(n-1)
class geoSeq {
  constructor(start, ratio, n=0) {
    this.a = start
    this.r = ratio
    this.n = n
  }

  //gets the nth term in the sequence (intended to be called by the object itself)
  tn(n=this.n) {
    return this.a * pow(this.r, (n - 1))
  }

  //gets the sum to the nth term including the nth term
  sn(n=this.n) {
    if (abs(n) > 1) {
      return this.a * ((pow(this.r, n) - 1) / (this.r - 1))
    } else {
      return this.a * ((1 - pow(this.r, n)) / (1 - this.r))
    }
  }

  //gets the limiting sum of the series where possible
  sInf() {
    if (abs(this.r) > 1) {
      throw "Series does not have a limiting sum"
    } else {
      return a / (1 - r)
    }
  }

  //gets the next term in the sequence and auto increments
  next() {
    let val = this.tn()
    this.n++
    return val
  }
}


//Object that tracks numerical functions and returns a culminative sum at a particular point
//Added functions must have one input and one return
class Superimposer {
  constructor() {
    this.functions = []
  }

  //add a new function to the set of functions
  addF(f) {
    this.functions.push(f)
  }

  //get the sum at point x
  compute(x) {
    let sum = 0
    for (let i = 0; i < this.functions.length; i++) {
      sum += this.functions[i](x)
    }
    return sum
  }
}


//Returns an array of x,y locations that are only integers that plot an area of a circle
function quantifyCircle(r) {
  var x = 0
  var y = 0
  var q1 = []
  var q2 = []
  var q3 = []
  var q4 = []
  while (pow(x, 2) + pow(y, 2) <= r) {
    while (pow(x, 2) + pow(y, 2) <= r) {
      q1.push([x, y])
      y++
    }
    y = 0
    x++
  }

  for (let i = 0; i < q1.length; i++) {
    let cp = q1[i]
    q2.push([cp[0]*-1, cp[1]])
    q3.push([cp[0], cp[1]*-1])
    q4.push([cp[0]*-1, cp[1]*-1])
  }

  return q1.concat(q2.concat(q3.concat(q4)))
}


function toRadians(a) {return a*(Math.PI/180);}
function toDegrees(a) {return a*(180/Math.PI);}


/*
  Cleanly gets a new perlin noise value, does iteration of the pos and
  lerps between two values all in one go.
  The pos is saved internally on this library file
*/
function getNoise(iter, lerpFrom=0, lerpTo=1) {
  _getNoise_pos += iter
  return lerp(lerpFrom, lerpTo, noise(_getNoise_pos))
}
