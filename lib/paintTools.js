var prevPt = null


function getPix(x, y) {
  let off = (y*width + x)*4
  let red = pixels[off]
  let green = pixels[off + 1]
  let blue = pixels[off + 2]
  let alpha = pixels[off + 3]
  return color(red, green, blue, alpha)
}


function setPix(x, y, col) {
  let off = (y*width + x)*4

  var d = pixelDensity()
  for (var i = 0; i < d; i++) {
    for (var j = 0; j < d; j++) {
      idx = 4 * ((y * d + j) * width * d + (x * d + i))
      pixels[idx] = red(col) + coeffr
      pixels[idx+1] = green(col) + coeffg
      pixels[idx+2] = blue(col) + coeffb
      pixels[idx+3] = alpha(col) + coeffa
    }
  }
}


function pixStroke(x, y, col) {
  setPix(x, y, col)
}


function crosshair(x, y, col) {
  for (let i = 0; i < width; i++) {
    setPix(i, y, col)
  }
  for (let i = 0; i < height; i++) {
    setPix(x, i, col)
  }
}


function lineTool(x, y, col, size=2) {
  if (prevPt) {
    stroke(col)
    strokeWeight(size)
    line(prevPt[0], prevPt[1], x, y)
    prevPt = null
  } else {
    prevPt = [x, y]
  }
}


function waterDrop(x, y, col, size=200) {
  var circlePts = quantifyCircle(size*5)
  var f = v => {return pow(1.0725, -v);}
  for (let i = 0; i < circlePts.length; i++) {
    let cp = circlePts[i]
    //console.log(255*f(sqrt(pow(cp[0], 2) + pow(cp[1], 2))))
    set(cp[0]+x, cp[1]+y, color(red(col), green(col), blue(col), 255*f(sqrt(pow(cp[0], 2) + pow(cp[1], 2)))))
    //set(cp[0]+x, cp[1]+y, col)
  }
}
