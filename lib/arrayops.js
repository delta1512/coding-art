/*
Set of array-related functions that make array processing much easier.
*/

function processBounds(x, y, limx, limy) {
  if (x > limx) {
    x -= limx
  } else if (x < 0) {
    x += limx
  }

  if (y > limy) {
    y -= limy
  } else if (y < 0) {
    y += limy
  }

  return [x, y]
}


function cartTo1D(x, y, width) {
  return (y * width + x)*4
}
