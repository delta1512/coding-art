/*
    A subclass of p5.Color which jerry-rigs the p5.Color constructor by passing in an
    object which satisfies what p5.Color desires.

    I know that the better way to do this is with the prototype chain but as of writing this
    I just need something that works. I'll learn it later :)
*/

class DynamicColor extends p5.Color {
    constructor(red, green, blue, alpha = 255) {
        super({
            _colorMode: _colorMode,
            _colorMaxes: _colorMaxes
        }, [red, green, blue, alpha])
    }

    getP5Color() {
        return color(this.levels)
    }

    addRed(v) {
        this.setRed((this._getRed() + v) % 256)
    }

    addGreen(v) {
        this.setGreen((this._getGreen() + v) % 256)
    }

    addBlue(v) {
        this.setBlue((this._getBlue() + v) % 256)
    }

    brighten(v) {
        this.addRed(v)
        this.addGreen(v)
        this.addBlue(v)
    }

    darken(v) {
        this.brighten(-v)
    }
}