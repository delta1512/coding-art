/*
An object that can track movement across a quantised plane (ie, an array).
The object automatically tracks limits and can optionally take a random step.
*/

class Walker {
  constructor(startx, starty, limitx, limity) {
    this.pos = createVector(startx, starty)
    this.limx = limitx
    this.limy = limity
    this.xoff = random(-10000, 10000)
    this.yoff = random(-10000, 10000)
  }

  move(vect) {
    this.pos.x += round(vect.x)
    this.pos.y += round(vect.y)

    let temp = processBounds(this.pos.x, this.pos.y, this.limx, this.limy)
    this.pos = createVector(temp[0], temp[1])
  }

  randWalk() {
    let rand1, rand2
    rand1 = random([-1, 0, 1])
    if (rand1 == 0) {
      rand2 = random([-1, 1])
    } else {
      rand2 = 0
    }

    if (random() > 0.5) {
      this.move(createVector(rand1, rand2))
    } else {
      this.move(createVector(rand2, rand1))
    }
  }

  randPWalk() {
    this.move(createVector(noise(this.xoff), noise(this.yoff)))
    this.xoff += 0.125
    this.yoff += 0.125
  }
}
