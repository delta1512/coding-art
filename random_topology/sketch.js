const nBezierNodes = 4
const nTrails = 10
const noiseIterator = 0.35
const noiseIntensity = 5
const overshoot = 100
const strokeSize = 35
const bgColour = '#3c0d82'
const strokeColour = '#5CFE01'

var yBezierOffset = 0
var xBezierOffset = 0
var baseTrail = new Trail2()


function setup() {
  createCanvas(windowWidth, windowHeight)
  frameRate(0)
  background(color(bgColour))
  stroke(color(strokeColour))
  strokeWeight(strokeSize)
  noFill()

  angleMode(DEGREES)
  rotate(random(0, 45))

  yBezierOffset = height / nBezierNodes
  xBezierOffset = width / nTrails

  // Set up ther initial trail
  for (let i = -overshoot; i < nBezierNodes + overshoot; i++) {
    baseTrail.addPoint(
      0 + getNoise(noiseIterator, -xBezierOffset, xBezierOffset) * noiseIntensity,
      yBezierOffset * i
    )
  }

  draw()
}

function draw() {
  baseTrail.drawBezier()

  let prevTrail = baseTrail

  // For every trail
  for (let i = 0; i < nTrails + overshoot; i++) {
    let tempTrail = new Trail2()
    let tempTrailStartX = xBezierOffset * (i + 1)
    // For every node in the trail
    for (let j = -overshoot; j < nBezierNodes + overshoot; j++) {
      let baseX = prevTrail.points[j + overshoot][0]

      tempTrail.addPoint(
        tempTrailStartX + baseX + getNoise(noiseIterator, -xBezierOffset, xBezierOffset) * noiseIntensity,
        yBezierOffset * j
      )
    }

    tempTrail.drawBezier()
    prevTrail = tempTrail
  }
}
