var wells = []
var planets = []
var nWells = 1
var nPlanets = 5
var maxWellMass = 1e14
var maxPlanetMass = 2e5


function setup() {
  createCanvas(windowWidth, windowHeight)

  for (let i = 0; i < nWells; i++) {
    wells.push(new PhysObj(random(width), random(height), random(maxWellMass), rcol(), true))
  }

  for (let i = 0; i < nPlanets; i++) {
    let x = random(width)
    let y = random(height)
    let newPlanet = new PhysObj(x, y, random(maxPlanetMass), rcol())
    newPlanet.trail = new Trail(x, y, x, y, 250)
    planets.push(newPlanet)
  }
}

function draw() {
  background(0)
  for (let i = 0; i < nWells; i++) {
    wells[i].show()
    for (let j = 0; j < nPlanets; j++) {
      let cPlanet = planets[j]
      cPlanet.applyField(gravity(wells[i], cPlanet))
    }
  }

  for (let k = 0; k < nPlanets; k++) {
    let cPlanet = planets[k]
    cPlanet.update()
    cPlanet.trail.addPoint(cPlanet.pos.x, cPlanet.pos.y, cPlanet.col)
    cPlanet.trail.draw()
    cPlanet.show()
  }
}


function rcol() {
  return color(random(255), random(255), random(255))
}
