
var colonies = []               //List for all colonies
const numC = 10                  //Number of starting colonies
var env

function setup() {
  createCanvas(windowWidth, windowHeight)
  background(0)
  frameRate(5)
  for (let i = 0; i < numC; i++) {
    newColony = new Colony()
    colonies.push(newColony)
    set(random(width), random(height), newColony.colour)
    updatePixels()
  }
  env = new Environment(350, 4000, 1/1725)
}


function draw() {
  loadPixels()
  env.update(frameCount)
  for (let x = 0; x < width-1; x++) {
    for (let y = 0; y < height-1; y++) {
      let i = cartTo1D(x, y, width)
      let currentCol = color(Array.from(pixels.slice(i, i+3)))
      if (currentCol.toString() != color(0).toString()) {
        let currentColony = findColony(currentCol)
        let neighbours = []
        let coord = []
        let c

        coord = [x    ,  y + 1]
        //console.log(coord)
        coord = processBounds(coord[0], coord[1], width, height)
        coord = cartTo1D(coord[0], coord[1], width)
        c = findColony(color(Array.from(pixels.slice(coord, coord+3))))
        c.coord = coord
        neighbours.push(c)

        coord = [x + 1,  y    ]
        coord = processBounds(coord[0], coord[1], width, height)
        coord = cartTo1D(coord[0], coord[1], width)
        c = findColony(color(Array.from(pixels.slice(coord, coord+3))))
        c.coord = coord
        neighbours.push(c)

        coord = [x    ,  y - 1]
        coord = processBounds(coord[0], coord[1], width, height)
        coord = cartTo1D(coord[0], coord[1], width)
        c = findColony(color(Array.from(pixels.slice(coord, coord+3))))
        c.coord = coord
        neighbours.push(c)

        coord = [x - 1,  y    ]
        coord = processBounds(coord[0], coord[1], width, height)
        coord = cartTo1D(coord[0], coord[1], width)
        c = findColony(color(Array.from(pixels.slice(coord, coord+3))))
        c.coord = coord
        neighbours.push(c)

        //skip if neighbours equal

        for (let j = 0; j < 4; j++) {
          let n = neighbours[j]
          if (currentColony != n) {
            let winner = env.computeTakeover(currentColony, n)
            if (winner) {
              //console.log(currentColony)
              //console.log(winner)
              if (winner.colour.toString() == currentColony.colour.toString()) {
                //console.log("won")
                pixels[n.coord] = red(currentColony.colour)
                pixels[n.coord + 1] = green(currentColony.colour)
                pixels[n.coord + 2] = blue(currentColony.colour)
              } else {
                //console.log("lost")
                pixels[i] = red(n.colour)
                pixels[i + 1] = green(n.colour)
                pixels[i + 2] = blue(n.colour)
              }
            }
          }
        }
      }
    }
  }
  updatePixels()
}


function findColony(col) {
  for (let i = 0; i < colonies.length; i++) {
    if (colonies[i].colour.toString() == col.toString()) {
      return colonies[i]
    }
  }
  return new Colony(color(0))
}
