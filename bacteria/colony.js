
class Colony {
  constructor(col=color(random(20, 255), random(20, 255), random(20, 255))) {
    this.colour = col
    this.dna = random(-10000, 10000)
    this.size = 1
    this.empty = (this.colour.toString() == color(0).toString())
  }

  mutate(enemyDna, win) {
    if (win) {
      if (enemyDna > this.dna) {
        this.dna -= random(500)
      } else {
        this.dna += random(500)
      }
      this.size += 1
    } else {
      if (enemyDna > this.dna) {
        this.dna += random(500)
      } else {
        this.dna -= random(500)
      }
      this.size -= 1
    }
  }
}
