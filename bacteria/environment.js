
const exp = 1.125               //Exponent used in probability calculation
const pEmpty = 0.25             //Probability of consuming empty space

class Environment {
  constructor(axis, coeff, speed) {
    this.axis = axis
    this.coeff = coeff
    this.speed = speed
    this.currentGoal = 0
  }

  update(t) {
    this.currentGoal = this.axis + this.coeff*sin(this.speed*t)
  }

  calcProb(dna) {
    if (dna < this.currentGoal) {
      return 1 - (-pow(exp, 0.05*(dna-this.currentGoal)) + 1)
    } else {
      return 1 - (-pow(exp, -0.05*(dna-this.currentGoal)) + 1)
    }
  }

  computeTakeover(colony1, colony2) {
    //where colony1 attempts to takeover colony2
    let first = colony1
    let second = colony2
    if (random() > 0.5) {
      first = colony2
      second = colony1
    }

    let p1 = this.calcProb(first.dna)
    let p2 = this.calcProb(second.dna)
    if (first.empty) {
      p1 = -1
      p2 = pEmpty
    }
    if (second.empty) {
      p1 = pEmpty
      p2 = -1
    }

    if (random() < p1) {
      return first
    } else if (random() < p2) {
      return second
    } else {
      return false
    }
  }
}
