var mainSeq
var graphline
var fx
var iter = 3
var x = 0
var xi = 0

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB)
  strokeWeight(6)
  mainSeq = new arithSeq(1, 1)
  graphline = new Trail(0, 0, 0, 0, width/iter)
  fx = new Superimposer()
  fx.addF((t) => {return 100*sin(PI*t/360)})
  fx.addF((t) => {return 50*cos(PI*t/180)})
  fx.addF((t) => {return 40*sin(PI*t/50 + 20)})
}

function draw() {
  background(0)
  translate(x, height/2)
  let c = color(360*abs(sin(PI*mainSeq.next()/360)), 100, 100)
  graphline.draw()
  graphline.addPoint(xi, fx.compute(x), c)
  x += iter
  xi -= iter
}
