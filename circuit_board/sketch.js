var tiles
var destinations = new Set()
var tracers = new Array()
var middleRows = new Array()
var outerEdges = new Array()
var seed
var im

const bgCol = '#0E7428' //'#000368' 
const tracerCol = '#268F40' //'#0279D8'
const res = 30
const nDestinations = 8
const resDiv = Math.floor(res / 3) // Used for determining where to place destinations
const maxTracerSize = res * 1.5


class Tracer {
    constructor(startIdxX, startIdxY, goalTile) {
        this.startX = startIdxX
        this.startY = startIdxY
        this.goalTile = goalTile
        this.currentHead = tiles.getTile(startIdxX, startIdxY)
        let head = this.currentHead.getCenter()
        head.push(color(tracerCol))
        this.trail = new Trail2([head], maxTracerSize)
    }

    step() {
        if (this.currentHead.idxDist(this.goalTile) == 0) {
            return
        }

        this.currentHead.used = true

        let neighbours = tiles.get8NeighboursByTile(this.currentHead).filter(x => x != null)

        neighbours = neighbours.filter(tile => !tile.used)

        if (neighbours.length == 0) {
            this.currentHead = this.goalTile
            return
        }

        neighbours.sort((a, b) => {
            return this.goalTile.idxDist(a) - this.goalTile.idxDist(b)
        })

        this.currentHead = neighbours.at(0)
        this.currentHead.used = true
        let head = this.currentHead.getCenter()
        this.trail.addPoint(head[0], head[1], color(tracerCol))
    }

    draw() {
        this.trail.draw()
    }
}


// function preload() {
//     im = loadImage('gfn.png')
// }


function setup() {
    createCanvas(windowWidth, windowHeight)
    frameRate(30)

    // Set up random seed
    seed = round(random(1, 500000))
    console.log(seed)
    randomSeed(seed)

    // Set up the tiles
    tiles = new Tiles(res, res)

    tiles.tiles.forEach(tiles_row => {
        tiles_row.forEach(tile => {
            // Tile config
            tile.col = color(bgCol)
            tile.pre_draw = () => {
                fill(tile.col)
                strokeWeight(0)
            }

            // Build arrays for destinations
            if ((tile.idx_x + 1) % resDiv == 0 && tile.idx_y > resDiv && tile.idx_y < res - resDiv && tile.idx_x != res - 1) {
                let choices = tiles.get8NeighboursByTile(tile).filter(x => x != null)
                choices.push(tile)
                middleRows.push(random(choices))
            
            // Build arrays for starts
            } else if (tile.idx_x == 0 || tile.idx_x == res - 1 || tile.idx_y == 0 || tile.idx_y == res - 1) { 
                outerEdges.push(tile)
            }
        })
    })

    // Choose the destinations
    for (let i = 0; i < nDestinations; i++) {
        destinations.add(random(middleRows))
    }

    // Set up tracers and starting location
    destinations.forEach(tile => {
        let start = random(outerEdges)
        tracers.push(new Tracer(start.idx_x, start.idx_y, tile))
    })

    background(color(bgCol))
}


function draw() {
    tiles.draw()

    strokeWeight(20)
    tracers.forEach(tracer => {
        tracer.step()
        tracer.draw()
    })
    
    // Mark the destinations
    destinations.forEach(t => {
        let center = t.getCenter()
        fill('#062d10')
        stroke('#fac94c')
        strokeWeight(5)
        circle(center[0], center[1], t.h)
    })

    //image(im, (width - 765) / 2, 0, 765, 972)
}