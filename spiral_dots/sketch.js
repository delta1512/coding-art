var x = 0
var y = 0
var a = 1
var s = 5
var colarr = []
var done = false

var xoff = 0
var yoff = 0

function setup() {
  createCanvas(windowWidth, windowHeight)
  //frameRate(1)
  colarr.push([255, 0, 0])
  colarr.push([0, 255, 0])
  colarr.push([0, 0, 255])
  //colarr.push([118, 0, 168])
  //colarr.push([34, 204, 0])

  xoff = windowWidth / 2
  yoff = windowHeight / 2
}


function draw() {
  //background(10)

  //if (!done) {
  //  for (let i = 0; i < 10000; i++) {
  fill(colarr[frameCount % colarr.length])
  ellipse(xoff + a * sin(x), yoff + a * cos(y), log(s))

  x += 0.125
  y += 0.125
  a += 0.125
  s += 0.125
  //  }

  //  done = true
  //}
}
