//function to fix scrollbars on the canvas
function windowResized() {
  resizeCanvas(1, 1)
  setTimeout(function() {resizeCanvas(windowWidth, windowHeight);}, 20)
}

function preload() {
  grcLogo = loadImage('https://raw.githubusercontent.com/gridcoin-community/Gridcoin-Marketing/master/Gridcoin%20Logos/PNG%20Format/Standalone%20Logos/GRCLogoOnly_Purple%26White_Solid.png')
}

function setup() {
  createCanvas(325, 325)
  speed = (1/175)*PI
}

var imgSize = 128
var radius = imgSize + 5
var speed = 0
var angle = 0
var electronSize = 20
var maxTrails = 500
var trailData = []

function draw() {
  for (let i = 0; i < 350; i++) {
    background(200)
    image(grcLogo, width/2-imgSize/2, height/2-imgSize/2, imgSize, imgSize)
    let x = radius * cos(angle) + (width/2)
    let y = radius * sin(angle) + (height/2)
    trail(x, y)
    noStroke()
    fill(0, 64, 255)
    ellipse(x, y, electronSize)
    angle += speed

    loadPixels()
    replacePixels(pixels, 200)
    updatePixels()
    saveCanvas('test.png')
  }
  noLoop()
}

function series(n) {return 255 - (255/maxTrails) * 2*(n - 1)}

function replacePixels(pixels, col) {
  for (var i = 0; i < 4*width*height; i += 4) {
    if ((pixels[i] + pixels[i + 1] + pixels[i + 2]) == 3*col) {
      pixels[i] = 0;
      pixels[i + 1] = 0;
      pixels[i + 2] = 0;
      pixels[i + 3] = 0;
    }
  }
}

function trail(x, y) {
  trailData.push([x, y])
  if (trailData.length > maxTrails) {
    trailData.splice(0, 1)
  }

  noFill()
  strokeWeight(3)
  trailData.reverse()
  if (trailData.length > 1) {
    let prevPoint = trailData[0]
    for (let i = 1; i < trailData.length; i++) {
      stroke(color(0, 0, 0, series(i)))
      line(prevPoint[0], prevPoint[1], trailData[i][0], trailData[i][1])
      prevPoint = trailData[i]
    }
  }
  trailData.reverse()
}
