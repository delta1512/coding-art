var background_colour = '#ffffff'

var rectPhys = []
var widthHeights = []
var min_w = 156
var min_h = 200
var max_w = 156
var max_h = 200
var min_a = 0
var max_a = 0
var n = 90
var v = 2

// For the symmetrical setup
var pad = 40
var n_per_row = 10
var n_per_col = 4


function setup() {
  createCanvas(windowWidth, windowHeight)
  //frameRate(1)
  min_w = (width - (n_per_row - 1)*pad) / n_per_row
  max_w = min_w
  min_h = (height - n_per_col*pad) / n_per_col
  max_h = min_h
  for (let i = 0; i < n; i++) {
    let c = color(random(255), random(255), random(255)) //random([color('#97F68A'), color('#D5BAEB')])
    let x = (((i * (min_w + pad)) % width) - floor((i * (min_w + pad)) / width) * pad) - 1000//random(0, width)
    let y = floor((i * (min_w + pad)) / width) * (min_h + pad) - 1000//random(0, height)
    let a = random(min_a, max_a)
    let direction = 1 //random([1, -1])
    let currPhys = new PhysObj(x, y, 1, c, false, 0, direction*v)
    currPhys.constrain = false
    currPhys.a.y = a
    rectPhys.push(currPhys)
    widthHeights.push([random(min_w, max_w), random(min_h, max_h)])
  }
}

function isOutside(idx) {
  let rp = rectPhys[idx]
  let wh = widthHeights[idx]

  if (rp.v.y > 0) {
    return rp.pos.y > height + 1000
  } else {
    return rp.pos.y + wh[1] < 0
  }
}

function draw() {
  background(background_colour)
  rotate(299.75)
  for (let i = 0; i < rectPhys.length; i++) {
    let rp = rectPhys[i]
    let wh = widthHeights[i]
    //console.log(wh)
    //console.log(rp.col)
    noStroke()
    fill(rp.col)
    rect(rp.pos.x, rp.pos.y, wh[0], wh[1])

    if (isOutside(i)) {
      let currWh = [random(min_w, max_w), random(min_h, max_h)]
      let c = color('#000000') //random([color('#97F68A'), color('#D5BAEB')]) //color(random(255), random(255), random(255))
      //let x = random(0, width)
      let y = -min_h - pad //-currWh[1]
      //rectPhys[i].pos.x = x
      rectPhys[i].pos.y = y
      rectPhys[i].col = c
      widthHeights[i] = currWh
    } else {
      // Dynamic colours!
      //let c = color((rp.col._getRed() + -0.5), (rp.col._getGreen() + -0.5), (rp.col._getBlue() + -0.5))
      //rectPhys[i].col = c
      rectPhys[i].update()
    }
  }
}
