var colInp
var opacity
var randomCheck
var toolBox

var PXL_B = 'Pixel brush'
var CRS_B = 'Crosshairs'
var LIN_B = 'Line tool'
var DRP_B = 'Water drop'


function setup() {
  createCanvas(500, 500)
  background(0, 0, 0, 0)
  createP('')
  colInp = createInput('#000000', 'color')
  opacity = createSlider(0, 255, 255)
  randomCheck = createCheckbox('Random colour')
  toolBox = createSelect()
  toolBox.option(PXL_B)
  toolBox.option(CRS_B)
  toolBox.option(LIN_B)
  toolBox.option(DRP_B)
}


function mousePressed() {
  if (mouseX <= 0 || mouseX > width || mouseY <= 0 || mouseY > height) {return;}

  loadPixels();
  if (randomCheck.checked()) {
    var brush = randcol(opacity.value())
  } else {
    var brush = color(colInp.value() + hex(opacity.value(), 2))
  }

  let b = toolBox.value()
  if (b == PXL_B) {
    pixStroke(mouseX, mouseY, brush)
  } else if (b == CRS_B) {
    crosshair(mouseX, mouseY, brush)
  } else if (b == LIN_B) {
    lineTool(mouseX, mouseY, brush)
    return
  } else if (b == DRP_B) {
    waterDrop(mouseX, mouseY, brush)
  }

  updatePixels();
}


function randcol(alpha=255) {
  return color(random(255), random(255), random(255), alpha)
}
