const initialResolution = 250 // Pixels (1:1 aspect ratio)
const levels = 1
const noiseCoefficient = 0.0075
const kernel = [[1, 1, 1 ], [ 1, 0, 1 ], [1, 1, 1 ]]

var noisePallet = []

function setup() {
  createCanvas(initialResolution, initialResolution)
  frameRate(20)

  setupNoisePallet()
}

function draw() {
    console.log(frameCount)
    loadPixels()
    setPixelsFromSimpleArray(noisePallet)
    updatePixels()

    filter(BLUR, 3)
    filter(THRESHOLD, (frameCount % 100) / 100)

    loadPixels()
    let edgeDetection = createSimpleEdgeDetection(pixelsToSimpleArray())
    // Black-out the last 3 rows of pixels because idk what's happening
    for (let i = width * (height - 3); i < width * height; i++) {
        edgeDetection[i] = 0
    }

    setPixelsFromSimpleArray(edgeDetection)
    updatePixels()
}

function fillNoise(threshold, noiseArray) {
    let final = []

    for (let i = 0; i < noiseArray.length; i++) {
        if (noiseArray[i] >= threshold) {
            final.push(255)
        } else {
            final.push(0)
        }
    }

    return final
}

function setupNoisePallet() {
    for (let x = 0; x < initialResolution; x++) {
        for (let y = 0; y < initialResolution; y++) {
          noisePallet.push(~~(noise(x * noiseCoefficient, y * noiseCoefficient) * 256))
        }
    }
}

/* Requires simple array, not pixel array. Requires black and white image */
function createSimpleEdgeDetection(fromArr) {
    let final = []

    for (let x = 1; x < width - 1; x++) {
        for (let y = 1; y < height - 1; y++) {
            let sum = 0

            for (kx = -1; kx <= 1; kx++) {
                for (ky = -1; ky <= 1; ky++) {

                    let xpos = x + kx
                    let ypos = y + ky

                    // This will either be 0 or 255
                    let val = fromArr[xpos + ypos * width]

                    sum += kernel[ky+1][kx+1] * val
                }
            }

            // We count an edge if there are 1-3 (inclusive) adjacent white pixels
            if (sum >= 255 && sum <= 765) {
                final.push(255)
            } else {
                final.push(0)
            }
        }
    }

    return final
}

function pixelsToSimpleArray() {
    let final = []

    for (let i = 0; i < width * height; i++) {
        let idx = i * 4
        final.push(pixels[idx])
    }

    return final
}

/*
    This can be improved when this issue makes it into the next build:
    https://github.com/processing/p5.js/issues/4993
*/
function setPixelsFromSimpleArray(arr) {
    for (let i = 0; i < arr.length; i++) {
        let idx = i * 4
        pixels[idx] = arr[i]
        pixels[idx + 1] = arr[i]
        pixels[idx + 2] = arr[i]
        pixels[idx + 3] = 255
    }
}
