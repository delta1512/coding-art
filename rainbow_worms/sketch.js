var worms = []
var n = 100

function setup() {
  createCanvas(windowWidth, windowHeight)
  for (let i = 0; i < n; i++) {
    worms.push(new Walker(random(width), random(height), width, height))
  }
}

function draw() {
  loadPixels()
  for (let w = 0; w < worms.length; w++) {
    let c = color(random(255), random(255), random(255))
    //let c = color(random(255))
    set(worms[w].pos.x, worms[w].pos.y, c)
    //worms[w].randWalk()
    //worms[w].move(createVector(0, 1))
    worms[w].randPWalk()
  }
  updatePixels()
}
