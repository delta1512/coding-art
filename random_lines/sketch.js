var lines = []
var decay_time = 15
var n = 20
var max_m = 5

function setup() {
  createCanvas(windowWidth, windowHeight)
  for (let i = 0; i < n; i++) {
    let c = color(random(255), random(255), random(255))
    //let c = color(random(255))
    lines.push(new Line(random(0, width), random(0, height), random([-max_m, max_m]), c, random(decay_time)))
  }
}

function draw() {
  //background(10)
  for (let i = 0; i < lines.length; i++) {
    lines[i].draw()
    lines[i].update()
    if (lines[i].old()) {
      let c = color(random(255), random(255), random(255))
      //let c = color(random(255))
      lines[i] = new Line(random(0, width), random(0, height), random(-max_m, max_m), c, random(decay_time))
    }
  }
}
