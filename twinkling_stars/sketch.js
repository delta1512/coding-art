var n = 150
var offset = 0
var twinkleSpeed = 0.00125
var stars = []


class Star {
    constructor(x=0, y=0, col=color(255), size=5, twinkleSpeedCoeff=1) {
        this.x = x
        this.y = y
        this.col = col
        this.size = size
        this.twinkleSpeedCoeff = twinkleSpeedCoeff
    }

    draw(offset) {
        stroke(this.col)
        strokeWeight(this.size * noise(offset * this.twinkleSpeedCoeff) - 0.125)
        point(this.x, this.y)
    }
}


function setup() {
  createCanvas(windowWidth, windowHeight)

  for (let i = 0; i < n; i++) {
      stars.push(new Star(random(0, width), random(0, height), color(255), random(3, 9), random(0, 1)))
  }
}

function draw() {
  background(0)

  for (let i = 0; i < stars.length; i++){
      offset += twinkleSpeed
      stars[i].draw(offset)
  }
}
