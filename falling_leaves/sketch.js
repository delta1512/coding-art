var leaves = []
var decay_time = 7
var n = 40
var gravity = new Field(9.8, 0, 90)
var wind = new Field(0, 0, 180)
var min_size = 10
var max_size = 30
var xoff = 0
var imgs = []
var underlay
var ul_width = 400
var ul_height = 400
var res_subdir = ''
var bg


function preload() {
  imgs.push(loadImage(res_subdir + 'snowflake1.png'))
  imgs.push(loadImage(res_subdir + 'snowflake2.png'))
  imgs.push(loadImage(res_subdir + 'snowflake3.png'))
  //imgs.push(loadImage('dollar1.png'))
  //imgs.push(loadImage('dollar2.png'))
  //imgs.push(loadImage('dollar3.png'))

  underlay = loadImage(res_subdir + 'delta.png')
  bg = loadImage(res_subdir + 'bg.jpg')
}


function setup() {
  createCanvas(500, 500)
  //frameRate(10)
  for (let i = 0; i < n; i++) {
    let c = color(20, 200, 20)

    let leaf = new PhysObj(random(0, width), 1, random(0.0125, 1), c, false, 0, 0)
    leaf.terminalV = random(1, 5)
    leaf.ttl = random(2, 4) * 1000
    leaf.constrain = false
    leaf.img = random(imgs)
    leaf.size = random(min_size, max_size)
    leaves.push(leaf)
  }
}

function draw() {
  //background(color('#e5fffe'))
  //background(5)
  image(bg, 0, 0, width, height)
  image(underlay, (width - ul_width)/2, (height - ul_height)/2, ul_width, ul_height)
  for (let i = 0; i < leaves.length; i++) {
    xoff += 0.0000125
    currentNoise = noise(xoff)
    wind = new Field(0, 2*currentNoise, 180*currentNoise)

    //leaves[i].show()
    image(leaves[i].img, leaves[i].pos.x, leaves[i].pos.y, leaves[i].size, leaves[i].size)
    leaves[i].applyField(gravity)
    leaves[i].applyField(wind)
    leaves[i].update()

    if (!leaves[i].isAlive() && leaves[i].pos.y >= height) {
      let c = color(20, 200, 20)
      let leaf = new PhysObj(random(0, width), 1, random(0.0125, 1), c, false, 0, 0)
      leaf.terminalV = random(1, 5)
      leaf.ttl = random(2, 4) * 1000
      leaf.constrain = false
      leaf.img = random(imgs)
      leaf.size = random(min_size, max_size)
      leaves[i] = leaf
    }
  }
}
