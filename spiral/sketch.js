const iter = 0.5
const aIter = 0.25
const length = 2500
const strokeCol = '#e58b57'
const bgCol = '#67b6ef'
const strokeSize = 20

var overlay


function preload() {
  overlay = loadImage('overlay.png')
}


function setup() {
  createCanvas(windowWidth, windowHeight)
  frameRate(60)
  background(0)

  angleMode(DEGREES)
}


function draw() {
    background(color(bgCol))

    translate(width / 2, height / 2)

    noFill()
    stroke(color(strokeCol))
    strokeWeight(strokeSize)
    beginShape()

    let a = 0

    for (var xy = 0; xy < length; xy += iter) {
        vertex(
            a * sin(xy - frameCount * 2),
            a * cos(xy - frameCount * 2)
        )

        a += aIter
    }

    endShape()

    image(overlay, -256, -256)
}
